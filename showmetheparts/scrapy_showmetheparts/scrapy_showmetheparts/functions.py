import urllib
import yaml
import pandas as pd

SETTINGS = 'smtp_settings.yaml'


def qry_smtp_encode(**kwargs):

    BASE = "http://www.showmetheparts.com/bin/ShowMeConnect.exe?"

    mapper = {
        'lookup': '',
        'engine': '',
        'year': '',
        'make': '',
        'model': '',
        'product': '',
        'id': 'SMTP2182',
        'storeid': '',
        'userid': '',
        '_dc': '',
        'page': '',
        'start': '',
        'limit': '',
        'callback': ''
    }

    for key, val in kwargs.iteritems():
        mapper[key] = val

    encode = urllib.urlencode(mapper)
    url = BASE + encode

    return url


def getURLParams(url, key):
    """
    URL = string
    key = list of strings
    """
    vals = {}

    for v in key:
        start = url.find(v)
        movement = start + len(v) + 1
        vals[v] = url[movement:movement + getFieldLen(v)]

    return vals


def getFieldLen(lookup):
    with open(SETTINGS, 'r') as stream:
        try:
            flen = yaml.load(stream)['lookup_field_len']
        except yaml.YAMLError as exc:
            print(exc)
    return flen[lookup]


def loadDeps(lookup):
    with open(SETTINGS, 'r') as stream:
        try:
            dep = yaml.load(stream)['lookup_dependencies']
        except yaml.YAMLError as exc:
            print(exc)

    return dep[lookup].split(',')


def loadFiles(lookup):
    with open(SETTINGS, 'r') as stream:
        try:
            dep = yaml.load(stream)['load_file']
        except yaml.YAMLError as exc:
            print(exc)

    return dep[lookup]


def getItertags(lookup):
    with open(SETTINGS, 'r') as stream:
        try:
            dep = yaml.load(stream)['itertags']
        except yaml.YAMLError as exc:
            print(exc)

    return dep[lookup]


def createMakeURLs(name):
    cols = loadDeps(name)
    dtypes = {c: str for c in cols}
    data = pd.read_csv(
        '../data/{}'.format(loadFiles(name)), usecols=cols, dtype=dtypes)
    years = data['year']
    return map(lambda x: qry_smtp_encode(lookup=name, year=x), list(years))


def createModelURLs(name):
    """years = list of years
       makes = list of make IDs
    """
    cols = loadDeps(name)
    dtypes = {c: str for c in cols}
    data = pd.read_csv(
        '../data/{}'.format(loadFiles(name)), usecols=cols, dtype=dtypes)
    years = list(data['year'])
    makes = list(data['id'].astype(str).str.zfill(getFieldLen('make')))

    return map(lambda x, y: qry_smtp_encode(lookup='model', year=x, make=y),
               years, makes)


def createProductsURLs(name):
    """years = list of years
       makes = list of make IDs
       models = list of model IDs
    """
    cols = loadDeps(name)
    dtypes = {c: str for c in cols}
    data = pd.read_csv(
        '../data/{}'.format(loadFiles(name)), usecols=cols, dtype=dtypes)
    years = list(data['year'])
    makes = list(data['make_id'].astype(str).str.zfill(getFieldLen('make')))
    models = list(data['id'].astype(str).str.zfill(getFieldLen('model')))

    return map(lambda x, y, z: qry_smtp_encode(lookup='product', year=x, make=y,
                                               model=z), years, makes, models)


def createEngineURLs(name):
    """years = list of years
       makes = list of make IDs
       models = list of model IDs
       products = list of product IDs
    """
    cols = loadDeps(name)
    dtypes = {c: str for c in cols}
    data = pd.read_csv(
        '../data/{}'.format(loadFiles(name)), usecols=cols, dtype=dtypes)
    years = list(data['year'])
    makes = list(data['make_id'].astype(str).str.zfill(getFieldLen('make')))
    models = list(data['model_id'].astype(str).str.zfill(getFieldLen('model')))
    products = list(data['id'].astype(str).str.zfill(getFieldLen('product')))

    return map(lambda a, b, c, d: qry_smtp_encode(lookup='engine',
                                                  year=a, make=b, model=c, product=d),
               years, makes, models, products)


def createPartsURLs(name):
    """years = list of years
       makes = list of make IDs
       models = list of model IDs
       products = list of product IDs
       engines = list of Engine IDs
    """
    cols = loadDeps(name)
    dtypes = {c: str for c in cols}
    data = pd.read_csv(
        '../data/{}'.format(loadFiles(name)), usecols=cols, dtype=dtypes)
    years = list(data['year'])
    makes = list(data['make_id'].astype(str).str.zfill(getFieldLen('make')))
    models = list(data['model_id'].astype(str).str.zfill(getFieldLen('model')))
    products = list(data['product_id'].astype(str).str.zfill(getFieldLen('product')))
    engines = list(data['id'].astype(str).str.zfill(getFieldLen('engine')))

    return map(lambda a, b, c, d, e: qry_smtp_encode(lookup='parts',
                                                     year=a, make=b, model=c, product=d, engine=e),
               years, makes, models, products, engines)


def createPartDetailURLs(name):
    """parts = list of part IDs
    """
    cols = loadDeps(name)
    dtypes = {c: str for c in cols}
    data = pd.read_csv(
        '../data/{}'.format(loadFiles(name)), usecols=cols, dtype=dtypes)
    parts = list(data['id'].astype(str).str.zfill(getFieldLen('part')))

    return map(lambda a: qry_smtp_encode(lookup='partdetails', part=a), parts)


def createPartImageURLs(name):
    """parts = list of part IDs
    """
    cols = loadDeps(name)
    dtypes = {c: str for c in cols}
    data = pd.read_csv(
        '../data/{}'.format(loadFiles(name)), usecols=cols, dtype=dtypes)
    parts = list(data['id'].astype(str).str.zfill(getFieldLen('part')))

    return map(lambda a: qry_smtp_encode(lookup='partimage', part=a), parts)
