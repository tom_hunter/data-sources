from scrapy.spiders import XMLFeedSpider

from scrapy_showmetheparts.items import OtherCatalogItem
from scrapy_showmetheparts.functions import getItertags

from lxml import etree as et

# import pdb


class OtherCatalogSpider(XMLFeedSpider):
    name = 'othercatalog'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']
    start_urls = ["http://www.showmetheparts.com/bin/ShowMeConnect.Exe?lookup=Othercatalog&id=SMTP2182"]
    iterator = 'iternodes'
    itertag = getItertags(name)

    def parse_node(self, response, node):
        i = OtherCatalogItem()
        i['id'] = et.XML(node.xpath('id').extract()[0]).text
        i['data'] = et.XML(node.xpath('data').extract()[0]).text
        return i
