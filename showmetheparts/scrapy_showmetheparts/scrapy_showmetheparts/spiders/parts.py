# from scrapy.spiders import XMLFeedSpider

# from scrapy_showmetheparts.items import PartsItem
# from scrapy_showmetheparts.functions import createPartsURLs
# from scrapy_showmetheparts.functions import getURLParams
# from scrapy_showmetheparts.functions import getItertags

# from lxml import etree as et


# class PartsSpider(XMLFeedSpider):
#     name = 'parts'
#     allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']
#     start_urls = createPartsURLs(name)
#     iterator = 'iternodes'
#     itertag = getItertags(name)

#     def parse_node(self, response, node):
#         i = PartsItem()
#         params = getURLParams(response.url, ['year', 'make', 'model', 'product', 'engine'])
#         i['year'] = params['year']
#         i['make_id'] = params['make']
#         i['model_id'] = params['model']
#         i['product_id'] = params['product']
#         i['engine_id'] = params['engine']
#         i['supplier'] = et.XML(node.xpath('supplier').extract()[0]).text
#         i['app_no'] = et.XML(node.xpath('app_no').extract()[0]).text
#         i['vehicle'] = et.XML(node.xpath('vehicle').extract()[0]).text
#         i['location'] = et.XML(node.xpath('location').extract()[0]).text
#         i['brand'] = et.XML(node.xpath('brand').extract()[0]).text
#         i['part_no'] = et.XML(node.xpath('part_no').extract()[0]).text
#         i['part_type'] = et.XML(node.xpath('part_type').extract()[0]).text
#         i['id'] = et.XML(node.xpath('part_key').extract()[0]).text
#         i['comment'] = et.XML(node.xpath('comment').extract()[0]).text
#         i['application'] = et.XML(node.xpath('application').extract()[0]).text
#         i['qty'] = et.XML(node.xpath('qty').extract()[0]).text
#         i['case_qty'] = et.XML(node.xpath('case_qty').extract()[0]).text
#         i['price'] = et.XML(node.xpath('price').extract()[0]).text
#         i['storelocator'] = et.XML(node.xpath('storelocator').extract()[0]).text
#         i['doc'] = et.XML(node.xpath('doc').extract()[0]).text
#         i['udf2'] = et.XML(node.xpath('udf2').extract()[0]).text
#         i['udf1'] = et.XML(node.xpath('udf1').extract()[0]).text
#         i['displayorder'] = et.XML(node.xpath('displayorder').extract()[0]).text
#         i['supplierid'] = et.XML(node.xpath('supplierid').extract()[0]).text
#         i['image'] = et.XML(node.xpath('image').extract()[0]).text
#         i['epicor_id'] = et.XML(node.xpath('epicor_id').extract()[0]).text
#         i['epicor_name'] = et.XML(node.xpath('epicor_name').extract()[0]).text
#         i['smtp_brandid'] = et.XML(node.xpath('smtp_brandid').extract()[0]).text
#         i['data'] = et.XML(node.xpath('data').extract()[0]).text
#         i['id2'] = et.XML(node.xpath('id').extract()[0]).text
#         i['linecode'] = et.XML(node.xpath('linecode').extract()[0]).text
#         return i
