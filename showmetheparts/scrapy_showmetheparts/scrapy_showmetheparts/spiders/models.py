from scrapy.spiders import XMLFeedSpider

from scrapy_showmetheparts.items import ModelItem
from scrapy_showmetheparts.functions import createModelURLs
from scrapy_showmetheparts.functions import getURLParams
from scrapy_showmetheparts.functions import getItertags

from lxml import etree as et


class ModelsSpider(XMLFeedSpider):
    name = 'model'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']
    start_urls = createModelURLs(name)
    iterator = 'iternodes'
    itertag = getItertags(name)

    def parse_node(self, response, node):
        i = ModelItem()
        params = getURLParams(response.url, ['year', 'make'])
        i['year'] = params['year']
        i['make_id'] = params['make']
        i['id'] = et.XML(node.xpath('id').extract()[0]).text
        i['model'] = et.XML(node.xpath('data').extract()[0]).text
        return i
