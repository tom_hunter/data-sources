from scrapy.spiders import XMLFeedSpider

from scrapy_showmetheparts.items import ProductsItem
from scrapy_showmetheparts.functions import createProductsURLs
from scrapy_showmetheparts.functions import getURLParams
from scrapy_showmetheparts.functions import getItertags

from lxml import etree as et


class ProductsSpider(XMLFeedSpider):
    name = 'product'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']
    start_urls = createProductsURLs(name)
    iterator = 'iternodes'
    itertag = getItertags(name)

    def parse_node(self, response, node):
        i = ProductsItem()
        params = getURLParams(response.url, ['year', 'make', 'model'])
        i['year'] = params['year']
        i['make_id'] = params['make']
        i['model_id'] = params['model']
        i['id'] = et.XML(node.xpath('id').extract()[0]).text
        i['product'] = et.XML(node.xpath('data').extract()[0]).text
        return i
