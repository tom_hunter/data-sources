from scrapy.spiders import XMLFeedSpider

from scrapy_showmetheparts.items import ServiceListItem
from scrapy_showmetheparts.functions import getItertags

from lxml import etree as et

# import pdb


class ServiceListSpider(XMLFeedSpider):
    name = 'servicelist'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']
    start_urls = [
        "http://www.showmetheparts.com/bin/ShowMeConnect.Exe?lookup=servicelist&id=SMTP2182"]
    iterator = 'iternodes'
    itertag = getItertags(name)

    def parse_node(self, response, node):
        i = ServiceListItem()
        i['category'] = et.XML(node.xpath('category').extract()[0]).text
        return i
