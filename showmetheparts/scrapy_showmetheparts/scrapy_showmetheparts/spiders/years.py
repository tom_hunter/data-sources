from scrapy.spiders import XMLFeedSpider

from scrapy_showmetheparts.items import YearItem
from scrapy_showmetheparts.functions import getItertags

from lxml import etree as et

# import pdb


class YearsSpider(XMLFeedSpider):
    name = 'year'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']
    start_urls = ["http://www.showmetheparts.com/bin/ShowMeConnect.Exe?lookup=year&id=SMTP2182"]
    iterator = 'iternodes'
    itertag = getItertags(name)

    def parse_node(self, response, node):
        # pdb.set_trace()
        i = YearItem()
        i['year'] = et.XML(node.xpath('id').extract()[0]).text
        return i
