from scrapy.spiders import XMLFeedSpider

from scrapy_showmetheparts.items import ExhYearItem
from scrapy_showmetheparts.functions import getItertags

from lxml import etree as et


class ExhYearSpider(XMLFeedSpider):
    name = 'exhyear'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']
    start_urls = ["http://www.showmetheparts.com/bin/ShowMeConnect.Exe?lookup=exhyear&id=SMTP2182"]
    iterator = 'iternodes'
    itertag = getItertags(name)

    def parse_node(self, response, node):
        i = ExhYearItem()
        i['exhyear'] = et.XML(node.xpath('id').extract()[0]).text
        return i
