from scrapy.spiders import XMLFeedSpider

from scrapy_showmetheparts.items import SupplierItem
from scrapy_showmetheparts.functions import getItertags

from lxml import etree as et


class SuppliersSpider(XMLFeedSpider):
    name = 'supplier'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']
    start_urls = ["http://www.showmetheparts.com/bin/ShowMeConnect.Exe?lookup=supplier&Supplier=Full&id=SMTP2182"]
    iterator = 'iternodes'
    itertag = getItertags(name)

    def parse_node(self, response, node):
        i = SupplierItem()
        i['data'] = et.XML(node.xpath('data').extract()[0]).text
        i['id'] = et.XML(node.xpath('id').extract()[0]).text
        i['linecode'] = et.XML(node.xpath('linecode').extract()[0]).text
        return i
