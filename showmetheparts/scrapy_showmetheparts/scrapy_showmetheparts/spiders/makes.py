from scrapy.spiders import XMLFeedSpider

from scrapy_showmetheparts.items import MakeItem
from scrapy_showmetheparts.functions import createMakeURLs
from scrapy_showmetheparts.functions import getURLParams
from scrapy_showmetheparts.functions import getItertags

from lxml import etree as et


class MakesSpider(XMLFeedSpider):
    name = 'make'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']
    start_urls = createMakeURLs(name)
    iterator = 'iternodes'
    itertag = getItertags(name)

    def parse_node(self, response, node):
        i = MakeItem()
        params = getURLParams(response.url, ['year'])
        i['year'] = params['year']
        i['id'] = et.XML(node.xpath('id').extract()[0]).text
        i['make'] = et.XML(node.xpath('data').extract()[0]).text
        return i
