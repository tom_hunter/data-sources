# from scrapy.spiders import XMLFeedSpider

# from scrapy_showmetheparts.items import EnginesItem
# from scrapy_showmetheparts.functions import createEngineURLs
# from scrapy_showmetheparts.functions import getURLParams
# from scrapy_showmetheparts.functions import getItertags

# from lxml import etree as et


# class EnginesSpider(XMLFeedSpider):
#     name = 'engine'
#     allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']
#     start_urls = createEngineURLs(name)
#     iterator = 'iternodes'
#     itertag = getItertags(name)

#     def parse_node(self, response, node):
#         i = EnginesItem()
#         params = getURLParams(response.url, ['year', 'make', 'model', 'product'])
#         i['year'] = et.XML(node.xpath('yearid').extract()[0]).text
#         i['year_req'] = params['year']
#         i['make_id'] = et.XML(node.xpath('makeid').extract()[0]).text
#         i['make_id_req'] = params['make']
#         i['model_id'] = et.XML(node.xpath('modelid').extract()[0]).text
#         i['model_id_req'] = params['model']
#         i['product_id'] = params['product']
#         i['engine_id'] = et.XML(node.xpath('id').extract()[0]).text
#         i['engine'] = et.XML(node.xpath('data').extract()[0]).text
#         i['engnumeric_id'] = et.XML(
#             node.xpath('engineid').extract()[0]).text
#         i['method_id'] = et.XML(node.xpath('methodid').extract()[0]).text
#         i['label'] = et.XML(node.xpath('label').extract()[0]).text
#         i['style'] = et.XML(node.xpath('style').extract()[0]).text
#         return i
