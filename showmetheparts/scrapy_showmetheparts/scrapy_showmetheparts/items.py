from scrapy import Item, Field


class SupplierItem(Item):
    data = Field()
    id = Field()
    linecode = Field()


class YearItem(Item):
    year = Field()


class ExhYearItem(Item):
    exhyear = Field()


class OtherCatalogItem(Item):
    id = Field()
    data = Field()


class ServiceListItem(Item):
    category = Field()


class MakeItem(Item):
    year = Field()
    id = Field()
    make = Field()


class ModelItem(Item):
    year = Field()
    make_id = Field()
    id = Field()
    model = Field()


class ProductsItem(Item):
    year = Field()
    make_id = Field()
    model_id = Field()
    id = Field()
    product = Field()


class EnginesItem(Item):
    year = Field()
    year_req = Field()
    make_id = Field()
    make_id_req = Field()
    model_id = Field()
    model_id_req = Field()
    product_id = Field()
    engine_id = Field()
    engine = Field()
    engnumeric_id = Field()
    method_id = Field()
    label = Field()
    style = Field()


class PartsItem(Item):
    year = Field()
    make_id = Field()
    model_id = Field()
    product_id = Field()
    engine_id = Field()
    supplier = Field()
    app_no = Field()
    vehicle = Field()
    location = Field()
    brand = Field()
    part_no = Field()
    part_type = Field()
    id = Field()
    comment = Field()
    application = Field()
    qty = Field()
    case_qty = Field()
    price = Field()
    storelocator = Field()
    doc = Field()
    udf2 = Field()
    udf1 = Field()
    displayorder = Field()
    supplierid = Field()
    image = Field()
    epicor_id = Field()
    epicor_name = Field()
    smtp_brandid = Field()
    data = Field()
    id2 = Field()
    linecode = Field()


class PartImagesItem(Item):
    part_id = Field()
    url = Field()
    height = Field()
    width = Field()


class PartDetailsItem(Item):
    part_id = Field()
    attribute = Field()
    value = Field()


class AppItem(Item):
    part_id = Field()
    make = Field()
    model = Field()
    year = Field()
    engine = Field()
    parttype = Field()
    apptype = Field()
