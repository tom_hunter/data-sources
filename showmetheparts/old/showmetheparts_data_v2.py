
# coding: utf-8

# In[1]:

import numpy as np
import pandas as pd
import json
from bs4 import BeautifulSoup
import urllib
import requests
from lxml import etree
import re
import time
import pdb
import sys


# ### -------URL CREATION FUNCTIONS-----------

# In[2]:

#deprecated by requests implementation
def httpgetreq(some_url):
    '''Basic http get request function'''
    sock = urllib.urlopen(some_url)
    content = sock.read()
    sock.close()
    return content


# In[3]:

#GLOBAL VERSION
def qry_smtp(stream=False,**kwargs):
    
    '''GENERIC SHOW ME THE PARTS "API" DATA REQUEST METHOD'''
    
    BASE = "http://www.showmetheparts.com/bin/ShowMeConnect.exe?"
    
    mapper = {
    'lookup': '',
    'engine': '',
    'year': '',
    'make': '',
    'model': '',
    'product': '',
    'id': '091681',
    'storeid': '',
    'userid': '',
    '_dc': '',
    'page': '',
    'start': '',
    'limit': '',
    'callback': ''
    }
    
    for key, val in kwargs.iteritems():
        mapper[key] = val

#     encode = urllib.urlencode(mapper)
#     url = BASE+encode

    r = requests.get(BASE, params=mapper, timeout=0.001)
    
    return r


# In[4]:

#GLOBAL VERSION
def qry_smtp_encode(**kwargs):
    
    '''GENERIC SHOW ME THE PARTS "API" DATA REQUEST METHOD'''
    
    BASE = "http://www.showmetheparts.com/bin/ShowMeConnect.exe?"
    
    mapper = {
    'lookup': '',
    'engine': '',
    'year': '',
    'make': '',
    'model': '',
    'product': '',
    'id': '091681',
    'storeid': '',
    'userid': '',
    '_dc': '',
    'page': '',
    'start': '',
    'limit': '',
    'callback': ''
    }
    
    for key, val in kwargs.iteritems():
        mapper[key] = val

    encode = urllib.urlencode(mapper)
    url = BASE+encode
    
    return url


# In[5]:

def add_zeros(df):
    '''Fixes ID values that dont have leading 0s'''
    digits = {1: '000',
              2: '00',
              3: '0',
              }
    searchterms = ['makeID', 'catalogID', 'modelID', 'productID']
    df_new = df
    for k,v in df.iteritems():
        if k in searchterms:
            values = []
            for val in v:
                try:
                    x = digits[len(str(val))] + str(val)
                    values.append(x)
                except:
                    #for type consistency in the list
                    values.append(str(val))
            df_new[k] = values
    return df_new


# In[6]:

#test building input list dynamically based on df 
x=5
for i in xrange(1,x,1):
    if i == 1:
        digits = {i: x * '0'}
    else:
        digits[i] = (x-(i-1)) * '0'
digits

def add_zeros_new(df, max_zeros=3):
#     NEED TO MODIFY TO TAKE ON FOUR POSSIBLE LEADING ZEROES
    '''Fixes ID values that dont have leading 0s'''
    values = []
    
#     builds digits dict based on max_zeros arg
#     for i in xrange(max_zeros):
#         digits = {i: 
    
    digits = {1: '000',
              2: '00',
              3: '0'
              }
    
    searchterms = ['makeID', 'catalogID', 'modelID', 'productID']
    df_new = df
    for k,v in df.iteritems():
        if k in searchterms:
            for val in v:
                try:
                    x = digits[len(str(val))] + str(val)
                    values.append(x)
#                 except KeyError:
                except:
                    #for type consistency in the list
                    values.append(str(val))
            pdb.set_trace()
            df_new[k] = values
    return df_new


# ### ----------------DATA LOADING FUNCTIONS------------------------

# In[7]:

def getSuppliers():
    """supplier data --> df_sup"""
    #list of all suppliers
    suppliers = []
    #dict of individual supplier data
    supplier = {}
    cnt = 0

    #feed url to return xml doc and load into etree
    data = qry_smtp(Supplier='Full', lookup='supplier')
    tree = etree.XML(data.content)

    #iterate over each element and create dicts based on values
    for element in tree.iter('data', 'linecode', 'id'):
        cnt += 1
        supplier.__setitem__(element.tag, element.text)

        if cnt == 3:
            suppliers.append(supplier)
            supplier = {}
            cnt = 0

    #finally, create a dataframe
    df_sup = pd.DataFrame(suppliers)
    rename = df_sup.columns.values
    rename[0] = 'supplier'
    rename[1] = 'supID'
    df_sup.columns = rename
    return df_sup

def getYears():
    """year --> df_year"""
    #list of all years
    years = []

    #feed url to return xml doc and load into etree
    data = qry_smtp(lookup='year')
    tree = etree.XML(data.content)

    #iterate over each element and create dicts based on values
    for element in tree.iter('id'):
        years.append(element.text)

    #finally, create a dataframe
    df_year = pd.DataFrame(years)
    return df_years

def getExhYears():
    """exhyear --> df_exhyear"""
    #list of all years
    values = []

    #feed url to return xml doc and load into etree
    data = qry_smtp(lookup='exhyear')
    tree = etree.XML(data.content)

    #iterate over each element and create dicts based on values
    for element in tree.iter('id'):
        values.append(element.text)

    #INPUT DF NAME
    df_exhyear = pd.DataFrame(values)
    return df_exhyear

def getServicers():
    """servicelist --> df_servicelist"""
    #list of all years
    values = []

    #feed url to return xml doc and load into etree
    data = qry_smtp(lookup='servicelist')
    tree = etree.XML(data.content)

    #iterate over each element and create dicts based on values
    for element in tree.iter('category'):
        values.append(element.text)

    #finally, create a dataframe
    df_servicelist = pd.DataFrame(values)
    return df_servicelist

def getOthercatalog():
    """Othercatalog --> df_Othercatalog"""
    #list of all suppliers
    values = []
    #dict of individual supplier data
    sub = {}
    cnt = 0

    #feed url to return xml doc and load into etree
    data = qry_smtp(lookup='Othercatalog')
    tree = etree.XML(data.content)

    #iterate over each element and create dicts based on values
    for element in tree.iter('data', 'id'):
        cnt += 1
        sub.__setitem__(element.tag, element.text)

        if cnt == 2:
            values.append(sub)
            sub = {}
            cnt = 0

    df_Othercatalog = pd.DataFrame(values)
    rename = df_Othercatalog.columns.values
    rename[0] = 'catalog'
    rename[1] = 'catalogID'
    df_Othercatalog.columns = rename
    return df_Othercatalog

def getMake(df_Year):
    """make --> df_make"""
     #create list of tuples of urls & years for use,
    year_url = map(lambda x: (x, qry_smtp_encode(lookup='make', year=x)), list(df_year[0]))

    #list of all sub dictionaries
    values = []

    #dict of individual sub element data
    sub = {}
    cnt = 0

    for year, url in year_url:

        #feed url to return xml doc and load into etree
        data = qry_smtp(stream=True, lookup='make', year=year)
        tree = etree.XML(data.content)

        #iterate over each element and create dicts based on values
        for element in tree.iter('id', 'data'):
            cnt += 1
            sub.__setitem__(element.tag, element.text)
            sub.__setitem__('year', year)

            if cnt == 2:
                values.append(sub)
                sub = {}
                cnt = 0

    #finally, create a dataframe
    df_make = pd.DataFrame(values)
    rename = df_make.columns.values
    rename[1] = 'makeID'
    df_make.columns = rename
    return df_make

def getModels(df_make):
    """model for each make/year combo --> df_model"""
    #create list of tuples of urls, years, makes for use
    year_make_url = map(lambda x, y: (x, y,qry_smtp_encode(lookup='model', year=x, make=y)),                         list(df_make['year']), list(df_make['makeID']))

    #list of all sub dictionaries
    values = []

    #dict of individual sub element data
    sub = {}
    cnt = 0
    cnter = 0

    for year, makeID, url in year_make_url:

        try:
            #feed url to return xml doc and load into etree
            data = requests.get(url)
            tree = etree.XML(data.content)

            #iterate over each element and create dicts based on values
            for el in tree.iter('id', 'data'):
    #             pdb.set_trace()
                cnt += 1
                sub[el.tag] = el.text
                sub['makeID'] = makeID
                sub['year'] = year

                if cnt == 2:
                    values.append(sub)
                    sub = {}
                    cnt = 0

            #track progress
            if cnter % 100 == 0:
                print "*",
                time.sleep(1)

            cnter+=1
        except Exception as e:
            print e
    #         pdb.set_trace()

    #finally, create a dataframe
    df_model = pd.DataFrame(values)
    rename = df_model.columns.values
    rename[0] = 'model'
    rename[1] = 'modelID'
    rename[2] = 'makeID'
    df_model.columns = rename
    return df_model

def getProducts(df_model):
    """products for each year/model/make combo --> df_products"""
    #create list of tuples of urls, years, makes, model for use
    year_make_model_url = map(lambda x, y, z: (x, y, z, qry_smtp_encode(lookup='product', year=x, make=y, model=z)),                               list(df_model['year']), list(df_model['makeID']), list(df_model['modelID']))

    #list of all sub dictionaries
    errors = []
    values = []

    #dict of individual sub element data
    sub = {}
    cnt = 0
    cnter = 0

    for year, make, model, url in year_make_model_url:

        try:

            #feed url to return xml doc and load into etree
            data = requests.get(url)
            tree = etree.XML(data.content)

            #iterate over each element and create dicts based on values
            for el in tree.iter('id', 'data'):
                cnt += 1
                sub[el.tag] = el.text
                #probably can clean up col names and then renaming bellow 
                sub['make'] = make
                sub['model'] = model
                sub['year'] = year

                if cnt == 2:
                    values.append(sub)
                    sub = {}
                    cnt = 0
            cnter+=1
            if cnter % 100 == 0:
                print "*",
                time.sleep(1)

        except:
            print "error"
            errors.append((year, make, model, url))
            pdb.set_trace()

    #finally, create a dataframe
    df_product = pd.DataFrame(values)
    rename = df_product.columns.values
    rename[1] = 'productID'
    rename[2] = 'makeID'
    rename[3] = 'modelID'
    df_product.columns = rename
    return df_product




# **Dump everything to CSV for safekeeping**

# In[8]:

def dumpToCSV():
    try:
        if 'win' in sys.platform:
            df_sup.to_csv('data\suppliers.csv')
            df_year.to_csv('data\years.csv')
            df_exhyear.to_csv('data\exhyears.csv')
            df_servicelist.to_csv('data\services.csv')
            df_Othercatalog.to_csv('data\othercatalog.csv')
            df_make.to_csv('data\make.csv')
            df_model.to_csv('data\model.csv')
            df_product.to_csv('data\product.csv')
            df_engine.to_csv('data\engine.csv')
            # df_parts.to_csv('data\parts.csv')
        else:
            df_sup.to_csv('data/suppliers.csv')
            df_year.to_csv('data/years.csv')
            df_exhyear.to_csv('data/exhyears.csv')
            df_servicelist.to_csv('data/services.csv')
            df_Othercatalog.to_csv('data/othercatalog.csv')
            df_make.to_csv('data/make.csv')
            df_model.to_csv('data/model.csv')
            df_product.to_csv('data/product.csv')
            df_engine.to_csv('data/engine.csv')
            # df_parts.to_csv('data/parts.csv')
            
            print "saved all files with no errors"
    except:
        print "error saving files"


# ### try loading values from cache'd csv files first

# In[9]:

try:
    if 'win' in sys.version: 
        #WINDOWS
        df_sup = pd.DataFrame.from_csv('data\suppliers.csv')
        df_year = pd.DataFrame.from_csv('data\years.csv')
        df_exhyear = pd.DataFrame.from_csv('data\exhyears.csv')
        df_servicelist = pd.DataFrame.from_csv('data\services.csv')
        df_Othercatalog = pd.DataFrame.from_csv('data\othercatalog.csv')
        df_make = pd.DataFrame.from_csv('data\make.csv')
        df_model = pd.DataFrame.from_csv('data\model.csv')
        df_product = pd.DataFrame.from_csv('data\product.csv')
        df_engine = pd.DataFrame.from_csv('data\engine.csv')
    else:
        #OSX/LINUX
        df_sup = pd.DataFrame.from_csv('data/suppliers.csv')
        df_year = pd.DataFrame.from_csv('data/years.csv')
        df_exhyear = pd.DataFrame.from_csv('data/exhyears.csv')
        df_servicelist = pd.DataFrame.from_csv('data/services.csv')
        df_Othercatalog = pd.DataFrame.from_csv('data/othercatalog.csv')
        df_make = pd.DataFrame.from_csv('data/make.csv')
        df_model = pd.DataFrame.from_csv('data/model.csv')
        df_product = pd.DataFrame.from_csv('data/product.csv')
        df_engine = pd.DataFrame.from_csv('data/engine.csv')

    file_list = [df_Othercatalog, df_make, df_model, df_product]

    for f in file_list:
        f = add_zeros(f)

    print "loaded all files!"
except:
    print "error, either load data directly from smtp or manually import"


# ## Load drop-down list values

# >engines for each year/model/make/product combo --> df_engines
# **just for storage "All~~~" returns the same data

# In[ ]:

#create list of tuples of urls, years, makes, model for use
year_make_model_product_url = map(lambda a, b, c, d: (a, b, c, d, qry_smtp_encode(lookup='engine',                         year=a, make=b, model=c, product=d)),                         list(df_product['year']), list(df_product['makeID']),                         list(df_product['modelID']), df_product['productID'])


#list of all sub dictionaries
values = []
errors = []

#dict of individual sub element data
sub = {}
cnt = 0
cnter = 0

for year, make, model, product, url in year_make_model_product_url:

    try:
        #feed url to return xml doc and load into etree
        data = requests.get(url)
        tree = etree.XML(data.content)

        #iterate over each element and create dicts based on values
        for el in tree.iter('id', 'data'):
            cnt += 1
            sub[el.tag] = el.text
            sub['makeID'] = make
            sub['modelID'] = model
            sub['productID'] = product
            sub['year'] = year

            if cnt == 2:
                values.append(sub)
                sub = {}
                cnt = 0
        cnter+=1
        if cnter % 100 == 0:
            print "{} values loaded".format(len(values))
        time.sleep(0.1)
        
        if cnter % 1000 == 0:
            #finally, create a dataframe
            df_engine = pd.DataFrame(values)
            dumpToCSV()
        
    except:
        errors.append(url)
        print "errors so far: {}, {} error rate".format(len(errors), float(len(errors)/cnter))

#finally, create a dataframe
df_engine = pd.DataFrame(values)
rename = df_engine.columns.values
# rename[1] = 'modelID'
# rename[2] = 'makeID'
df_engine.columns = rename
dumpToCSV()


# >parts data for each year/make/model/product/engine --> df_parts

# In[ ]:

# #create list of tuples of urls, years, makes, model for use
# year_make_model_product_url = map(lambda x, y, z: (x, y, z, w, qry_smtp_encode(lookup='product', year=x, make=y, model=z)),                           list(df_make['year']), list(df_make['makeID']), list(df_model['modelID']))

# ##FOR DATA INTEGRITY TESTING
# year_make_model_url2 = map(lambda x, y, z: (x, y, z, qry_smtp_encode(lookup='product', year=x, make=y, model=z)),                           list(df_model['year']), list(df_model['makeID']), list(df_model['modelID']))

# #list of all sub dictionaries
# values = []

# #dict of individual sub element data
# sub = {}
# cnt = 0
# cnter = 0

# for year, make, model, product, url in year_make_model_product_url:

#     try:
#         #feed url to return xml doc and load into etree
#         data = requests.get(url)
#         tree = etree.XML(data.content)

#         #iterate over each element and create dicts based on values
#         for el in tree.iter('id', 'data'):
#             cnt += 1
#             sub[el.tag] = el.text
#             sub['make'] = make
#             sub['model'] = model
#             sub['year'] = year
#             sub

#             if cnt == 2:
#                 values.append(sub)
#                 sub = {}
#                 cnt = 0
#         cnter+=1
#         if cnter % 100 == 0:
#             print "*",
#             time.sleep(1)

#     except:
#         print "error"
#         pdb.set_trace()

# #finally, create a dataframe
# df_parts = pd.DataFrame(values)
# rename = df_product.columns.values
# # rename[1] = 'modelID'
# # rename[2] = 'makeID'
# df_parts.columns = rename
# dumpToCSV()

