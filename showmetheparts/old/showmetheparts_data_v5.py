
# coding: utf-8

# In[1]:

import numpy as np
import pandas as pd
import json
import urllib
import requests
from lxml import etree
import time
import pdb
import sys
import pymysql
from sqlalchemy import create_engine

class SMTP(object):

    def __init__(self):
        pass

    # ### -------URL CREATION FUNCTIONS-----------

    # In[2]:

    #deprecated by requests implementation
    def httpgetreq(some_url):
        '''Basic http get request function'''
        sock = urllib.urlopen(some_url)
        content = sock.read()
        sock.close()
        return content

    #GLOBAL VERSION
    def qry_smtp(stream=False,**kwargs):
        
        '''GENERIC SHOW ME THE PARTS "API" DATA REQUEST METHOD'''
        
        BASE = "http://www.showmetheparts.com/bin/ShowMeConnect.exe?"
        
        mapper = {
        'lookup': '',
        'engine': '',
        'year': '',
        'make': '',
        'model': '',
        'product': '',
        'id': '091681',
        'storeid': '',
        'userid': '',
        '_dc': '',
        'page': '',
        'start': '',
        'limit': '',
        'callback': ''
        }
        
        for key, val in kwargs.iteritems():
            mapper[key] = val

        r = requests.get(BASE, params=mapper, timeout=0.001)
        
        return r

    def qry_smtp_encode(**kwargs):
        
        '''GENERIC SHOW ME THE PARTS "API" DATA REQUEST METHOD'''
        
        BASE = "http://www.showmetheparts.com/bin/ShowMeConnect.exe?"
        
        mapper = {
        'lookup': '',
        'engine': '',
        'year': '',
        'make': '',
        'model': '',
        'product': '',
        'id': '091681',
        'storeid': '',
        'userid': '',
        '_dc': '',
        'page': '',
        'start': '',
        'limit': '',
        'callback': ''
        }
        
        for key, val in kwargs.iteritems():
            mapper[key] = val

        encode = urllib.urlencode(mapper)
        url = BASE+encode
        
        return url

    def add_zeros(df):
        #DEPRECATE WITH STRING.ZFILL
        '''Fixes ID values that dont have leading 0s'''
        digits = {1: '000',
                  2: '00',
                  3: '0',
                  }
        
        searchterms = ['makeID', 'catalogID', 'modelID', 'productID', '']
        df_new = df
        for k,v in df.iteritems():
            if k in searchterms:
                values = []
                for val in v:
                    try:
                        x = digits[len(str(val))] + str(val)
                        values.append(x)
                    except:
                        #for type consistency in the list
                        values.append(str(val))
                df_new[k] = values
        return df_new

    def valuesdict(df, badcols=[]):
        data = {}
        goodcols = []
        goodcols = [col for col in df.columns if col not in badcols]
            
        for gc in goodcols:
            data.__setitem__(gc, df.to_dict()[gc].values())
        
        return data

    def chk_exsting_vals(df_target, df_lkup_vals, incl_cols=[], dropcols=[]):
        '''Compares two dfs and returns df_scrubbed after removing all similar df_target values in incl_cols'''
        
        #create dict of existing data in df_engine and a copy of df_product which we will use only for lookups            
        try:
            incl_cols[0]
            drop_cols[0]
        except IndexError:
            incl_cols = ['productID','makeID','modelID','year']
            drop_cols = ['id','data']

        values = valuesdict(df_target, badcols=drop_cols)
        df_lkup_vals = pd.DataFrame(df_lkup_vals, columns=incl_cols)

        #check if dict is in df_product and create row_mask with results in all columns
        df_scrubbed = df_lkup_vals[~df_lkup_vals.isin(values).all(1)]
        return df_scrubbed

    def appendvals(df_new, df_to_add, replace=False, vals=None):
        if df_new is None and replace==False:
            df_new = pd.DataFrame(vals)
        elif df_new is None and replace==True:
            df_new = df_to_add
        else:
            df_new.append(df_to_add, ignore_index=True)
        return df_new

    def multColSQLString(columns):
        """Takes in a given list of columns and returns a string for use in an INSERT sql statement 
            E.g. ['col1', 'col2', 'col3'] --> '`col1`, `col2`, `col3`"""
        
        plc_hld = '{:s}'
        s = ''
        
        for col in enumerate(columns):
            s += '`' + str(col[1]) + '`,'
        
        return s.rpartition(',')[0]

    # def dbconnect(db, host='localhost', user='user', password='password', charset='utf8mb4', config='../db/config.json'):
    #     conf = json.load(open(config))  
    #     return pymysql.connect(host=host,
    #                              user=conf[user],
    #                              password=conf[password],
    #                              db=db,
    #                              charset=charset,
    #                              cursorclass=pymysql.cursors.DictCursor)

    def dbconnect(db, host='localhost', user='user', password='password', charset='utf8mb4', config='../db/config.json'):
        conf = json.load(open(config))
        connection_string = 'mysql+pymysql://{:s}:{:s}@{:s}/{:s}'.format(conf[user], conf[password], host, db)
        engine = create_engine(connection_string,encoding='utf8',echo=True)  
        return engine


    # ### ----------------DATA LOADING FUNCTIONS------------------------

    # In[3]:

    def getSuppliers():
        """supplier data --> df_sup"""
        #list of all suppliers
        suppliers = []
        #dict of individual supplier data
        supplier = {}
        cnt = 0

        #feed url to return xml doc and load into etree
        data = qry_smtp(Supplier='Full', lookup='supplier')
        tree = etree.XML(data.content)

        #iterate over each element and create dicts based on values
        for element in tree.iter('data', 'linecode', 'id'):
            cnt += 1
            supplier.__setitem__(element.tag, element.text)

            if cnt == tree.items()[2] * 2:
                suppliers.append(supplier)
                supplier = {}
                cnt = 0

        #finally, create a dataframe
        df_sup = pd.DataFrame(suppliers)
        rename = df_sup.columns.values
        rename[0] = 'supplier'
        rename[1] = 'supID'
        df_sup.columns = rename
        return df_sup

    def getYears():
        """year --> df_year"""
        #list of all years
        years = []

        #feed url to return xml doc and load into etree
        data = qry_smtp(lookup='year')
        tree = etree.XML(data.content)

        #iterate over each element and create dicts based on values
        for element in tree.iter('id'):
            years.append(element.text)

        #finally, create a dataframe
        df_year = pd.DataFrame(years)
        return df_years

    def getExhYears():
        """exhyear --> df_exhyear"""
        #list of all years
        values = []

        #feed url to return xml doc and load into etree
        data = qry_smtp(lookup='exhyear')
        tree = etree.XML(data.content)

        #iterate over each element and create dicts based on values
        for element in tree.iter('id'):
            values.append(element.text)

        #INPUT DF NAME
        df_exhyear = pd.DataFrame(values)
        return df_exhyear

    def getServicers():
        """servicelist --> df_servicelist"""
        #list of all years
        values = []

        #feed url to return xml doc and load into etree
        data = qry_smtp(lookup='servicelist')
        tree = etree.XML(data.content)

        #iterate over each element and create dicts based on values
        for element in tree.iter('category'):
            values.append(element.text)

        #finally, create a dataframe
        df_servicelist = pd.DataFrame(values)
        return df_servicelist

    def getOthercatalog():
        """Othercatalog --> df_Othercatalog"""
        #list of all suppliers
        values = []
        #dict of individual supplier data
        sub = {}
        cnt = 0

        #feed url to return xml doc and load into etree
        data = qry_smtp(lookup='Othercatalog')
        tree = etree.XML(data.content)

        #iterate over each element and create dicts based on values
        for element in tree.iter('data', 'id'):
            cnt += 1
            sub.__setitem__(element.tag, element.text)

            if cnt == tree.items()[2] * 2:
                values.append(sub)
                sub = {}
                cnt = 0

        df_Othercatalog = pd.DataFrame(values)
        rename = df_Othercatalog.columns.values
        rename[0] = 'catalog'
        rename[1] = 'catalogID'
        df_Othercatalog.columns = rename
        return df_Othercatalog

    def getMake(df_Year):
        """make --> df_make"""
         #create list of tuples of urls & years for use,
        year_url = map(lambda x: (x, qry_smtp_encode(lookup='make', year=x)), list(df_year[0]))

        #list of all sub dictionaries
        values = []

        #dict of individual sub element data
        sub = {}
        cnt = 0

        for year, url in year_url:

            #feed url to return xml doc and load into etree
            data = qry_smtp(stream=True, lookup='make', year=year)
            tree = etree.XML(data.content)

            #iterate over each element and create dicts based on values
            for element in tree.iter('id', 'data'):
                cnt += 1
                sub.__setitem__(element.tag, element.text)
                sub.__setitem__('year', year)

                if cnt == tree.items()[2] * 2:
                    values.append(sub)
                    sub = {}
                    cnt = 0

        #finally, create a dataframe
        df_make = pd.DataFrame(values)
        rename = df_make.columns.values
        rename[1] = 'makeID'
        df_make.columns = rename
        return df_make

    def getModels(df_make):
        """model for each make/year combo --> df_model"""
        #create list of tuples of urls, years, makes for use
        year_make_url = map(lambda x, y: (x, y,qry_smtp_encode(lookup='model', year=x, make=y)),                         list(df_make['year']), list(df_make['makeID']))

        #list of all sub dictionaries
        values = []

        #dict of individual sub element data
        sub = {}
        cnt = 0
        cnter = 0

        for year, makeID, url in year_make_url:

            try:
                #feed url to return xml doc and load into etree
                data = requests.get(url)
                tree = etree.XML(data.content)

                #iterate over each element and create dicts based on values
                for el in tree.iter('id', 'data'):
        #             pdb.set_trace()
                    cnt += 1
                    sub[el.tag] = el.text
                    sub['makeID'] = makeID
                    sub['year'] = year

                    if cnt == tree.items()[2] * 2:
                        values.append(sub)
                        sub = {}
                        cnt = 0

                #track progress
                if cnter % 100 == 0:
                    print "*",
                    time.sleep(1)

                cnter+=1
            except Exception as e:
                print e
        #         pdb.set_trace()

        #finally, create a dataframe
        df_model = pd.DataFrame(values)
        rename = df_model.columns.values
        rename[0] = 'model'
        rename[1] = 'modelID'
        rename[2] = 'makeID'
        df_model.columns = rename
        return df_model

    def getProducts(df_model):
        """products for each year/model/make combo --> df_products"""
        #create list of tuples of urls, years, makes, model for use
        year_make_model_url = map(lambda x, y, z: (x, y, z, qry_smtp_encode(lookup='product', year=x, make=y, model=z)),                               list(df_model['year']), list(df_model['makeID']), list(df_model['modelID']))

        #list of all sub dictionaries
        errors = []
        values = []

        #dict of individual sub element data
        sub = {}
        cnt = 0
        cnter = 0

        for year, make, model, url in year_make_model_url:

            try:

                #feed url to return xml doc and load into etree
                data = requests.get(url)
                tree = etree.XML(data.content)

                #iterate over each element and create dicts based on values
                for el in tree.iter('id', 'data'):
                    cnt += 1
                    sub[el.tag] = el.text
                    #probably can clean up col names and then renaming bellow 
                    sub['make'] = make
                    sub['model'] = model
                    sub['year'] = year

                    if cnt == tree.items()[2] * 2:
                        values.append(sub)
                        sub = {}
                        cnt = 0
                cnter+=1
                if cnter % 100 == 0:
                    print "*",
                    time.sleep(1)

            except:
                print "error"
                errors.append((year, make, model, url))
                pdb.set_trace()

        #finally, create a dataframe
        df_product = pd.DataFrame(values)
        rename = df_product.columns.values
        rename[1] = 'productID'
        rename[2] = 'makeID'
        rename[3] = 'modelID'
        df_product.columns = rename
        return df_product

    def getEngines(df_product):
        """>engines for each year/model/make/product combo --> df_engines
        **just for storage "All~~~" returns the same data"""
        try:
            df_product_new = chk_exsting_vals(df_engine, df_product)
        except NameError:
            df_product_new = df_product

        #create list of tuples of urls, years, mak`es, model from df_product
        year_make_model_product_url = map(lambda a, b, c, d: (a, b, c, d, qry_smtp_encode(lookup='engine',                             year=a, make=b, model=c, product=d)),                             list(df_product_new['year']), list(df_product_new['makeID']),                             list(df_product_new['modelID']), list(df_product_new['productID']))

        #list of all sub dictionaries
        values = []

        #dict of individual sub element data and counters to track each element and overall progress
        sub = {}
        cnt = 0
        cnter = 0


        for year, make, model, product, url in year_make_model_product_url:
            #pdb.set_trace()
            try:
                #feed url to return xml doc and load into etree
                data = requests.get(url)
                tree = etree.XML(data.content)

                #iterate over each element and create dicts based on values
                el_list = ['data', 'id', 'yearid', 'makeid', 'modelid', 'engineid', 'methodid', 'label', 'style']
                for el in tree.iter(el_list):
                    cnt += 1
                    sub[el.tag] = el.text

                    if cnt == len(el_list):
                        values.append(sub)
                        sub = {}
                        cnt = 0
                cnter+=1         

                if cnter % 1000 == 0:
                    #finally, create a dataframe
                    df_wrking = pd.DataFrame(values)
                    print "{:d} values loaded".format(len(values))
                    time.sleep(0.1)
                    dumpToCSV()

            except:
                print "ERROR******{:s}".format(url)

        #finally, create a dataframe
        df_engine = appendvals(df_engine, df_wrking, replace=True, vals=values)
        dumpToCSV()


    # ### try loading values from cache'd csv files first

    # In[ ]:

    def dumpToCSV():
        try:
            if 'Apple' in sys.version:
                df_sup.to_csv('data/suppliers.csv')
                df_year.to_csv('data/years.csv')
                df_exhyear.to_csv('data/exhyears.csv')
                df_servicelist.to_csv('data/services.csv')
                df_Othercatalog.to_csv('data/othercatalog.csv')
                df_make.to_csv('data/make.csv')
                df_model.to_csv('data/model.csv')
                df_product.to_csv('data/product.csv')
                df_engine.to_csv('data/engine.csv')
                df_parts.to_csv('data/parts.csv')
                df_wrking.to_csv('data/wrking.csv')
            else:
                df_sup.to_csv('data\suppliers.csv')
                df_year.to_csv('data\years.csv')
                df_exhyear.to_csv('data\exhyears.csv')
                df_servicelist.to_csv('data\services.csv')
                df_Othercatalog.to_csv('data\othercatalog.csv')
                df_make.to_csv('data\make.csv')
                df_model.to_csv('data\model.csv')
                df_product.to_csv('data\product.csv')
                df_engine.to_csv('data\engine.csv')
                df_parts.to_csv('data\parts.csv')
                df_wrking.to_csv('data\wrking.csv')
                
                print "saved successfully"
        except:
            e = sys.exc_info()
            print e
            
    def loadDB():
        # Connect to the database
        conf = json.load(open("../db/config.json"))
        db = pymysql.connect(host='104.196.41.149',
                                     user=conf['user'],
                                     password=conf['password'],
                                     db='data_sources',
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)
        
        df_engine.to_sql('smtp_engine', db, flavor='mysql', if_exists='replace', chunksize=1000)
        df_make.to_sql('smtp_make', db, flavor='mysql', if_exists='replace', chunksize=1000)
        df_product.to_sql('smtp_product', db, flavor='mysql', if_exists='replace', chunksize=1000)
        df_model.to_sql('smtp_model', db, flavor='mysql', if_exists='replace', chunksize=1000)
        df_year.to_sql('smtp_year', db, flavor='mysql', if_exists='replace', chunksize=1000)
        df_Othercatalog.to_sql('smtp_Othercatalog', db, flavor='mysql', if_exists='replace', chunksize=1000)
        df_servicelist.to_sql('smtp_servicelist', db, flavor='mysql', if_exists='replace', chunksize=1000)
        df_exhyear.to_sql('smtp_exhaustyear', db, flavor='mysql', if_exists='replace', chunksize=1000)
        df_sup.to_sql('smtp_supplier', db, flavor='mysql', if_exists='replace', chunksize=1000)
        df_parts.to_sql('smtp_supplier', db, flavor='mysql', if_exists='replace', chunksize=1000)
        
    def loadData():
        col_mapper = {  'data': object,
                        'engine': object,
                        'engineID': object,
                        'label': object,
                        'makeID': object,
                        'methodID': object,
                        'modelID': object,
                        'style': object,
                        'year': np.int64,
                        'supplier':object,
                        'supID':object,
                        'linecode': object,
                        '0': object,
                        'catalog': object,
                        'catalogID': object,
                        'productID': object
                    }

        try:
            if 'Apple' in sys.version: 
                #OSX/LINUX
                df_sup = pd.read_csv('data/suppliers.csv', index_col=0, dtype=col_mapper)
                df_year = pd.read_csv('data/years.csv', index_col=0, dtype=col_mapper)
                df_exhyear = pd.read_csv('data/exhyears.csv', index_col=0, dtype=col_mapper)
                df_servicelist = pd.read_csv('data/services.csv', index_col=0, dtype=col_mapper)
                df_Othercatalog = pd.read_csv('data/othercatalog.csv', index_col=0, dtype=col_mapper)
                df_make = pd.read_csv('data/make.csv', index_col=0, dtype=col_mapper)
                df_model = pd.read_csv('data/model.csv', index_col=0, dtype=col_mapper)
                df_product = pd.read_csv('data/product.csv', index_col=0, dtype=col_mapper)
                df_engine = pd.read_csv('data/engine.csv', index_col=0, dtype=col_mapper)
                df_wrking = pd.read_csv('data/wrking.csv', index_col=0, dtype=col_mapper)
                df_parts = pd.read_csv('data/parts.csv', index_col=0, dtype=col_mapper)
            else:
                #WINDOWS
                df_sup = pd.read_csv('data\suppliers.csv', index_col=0, dtype=col_mapper)
                df_year = pd.read_csv('data\years.csv', index_col=0, dtype=col_mapper)
                df_exhyear = pd.read_csv('data\exhyears.csv', index_col=0, dtype=col_mapper)
                df_servicelist = pd.read_csv('data\services.csv', index_col=0, dtype=col_mapper)
                df_Othercatalog = pd.read_csv('data\othercatalog.csv', index_col=0, dtype=col_mapper)
                df_make = pd.read_csv('data\make.csv', index_col=0, dtype=col_mapper)
                df_model = pd.read_csv('data\model.csv', index_col=0, dtype=col_mapper)
                df_product = pd.read_csv('data\product.csv', index_col=0, dtype=col_mapper)
                df_engine = pd.read_csv('data\engine.csv', index_col=0, dtype=col_mapper)
                df_wrking = pd.read_csv('data\wrking.csv', index_col=0, dtype=col_mapper)
                df_parts = pd.read_csv('data\parts.csv', index_col=0, dtype=col_mapper)
            
            file_list = [df_Othercatalog, df_make, df_model, df_product, df_engine, df_wrking]

            for f in file_list:
                f = add_zeros(f)
                
            df_year.columns = ['year', 'source']
            df_exhyear.columns = ['year', 'source']
            df_servicelist.columns = ['service_tree', 'source']
            df_Othercatalog.columns = ['catalog', 'catalogID', 'source']
            df_make.columns = ['make', 'makeID', 'year', 'source']
            df_model.columns = ['model', 'modelID', 'makeID', 'year', 'source']
            df_product.columns = ['product', 'productID', 'makeID', 'modelID', 'year', 'source']
            df_engine.columns = ['engine','engineIDNum','engineID','label','makeID','methodID','modelID','style','year','source']
            
        #     df_list = {'year': df_year, 
        #            'suppliers': df_sup, 
        #            'exhaust_year': df_exhyear, 
        #            'service_list': df_servicelist,
        #            'other_catalog': df_Othercatalog,
        #            'make': df_make,
        #            'model': df_model,
        #            'product': df_product,
        #            'engine': df_engine,
        #            'part': df_part,
        #            'partimage': df_partimage, 
        #            'partdetail': df_partdetail, 
        #            'app': df_app, 
        #            'doc': df_doc, 
        #            'kit': df_kit, 
        #            'swf': df_swf, 
        #            '360IMAGES': df_360IMAGES
        #           }

            # dumpToCSV()

            return df_sup, df_year
            # return df_exhyear
            # return df_servicelist
            # return df_Othercatalog
            # return df_make
            # return df_model
            # return df_product
            # return df_parts
            # return df_engine
            # return df_wrking
        except:
            e = sys.exc_info()
            df_wrking = pd.DataFrame()
            print e


    # In[ ]:

    # loadDB()


    # # ## Load drop-down list values

    # # >parts data for each year/make/model/product/engine --> df_parts

    # # In[ ]:

    # df_product_new = df_product
    # # try:
    # #     df_product_new = chk_exsting_vals(df_parts, df_engine)
    # # except NameError:
    # #     df_product_new = df_product

    # #create list of tuples of urls, years, mak`es, model from df_product
    # year_make_model_product_engine_url = map(lambda a, b, c, d: (a, b, c, d, qry_smtp_encode(lookup='parts',                                          year=a, make=b, model=c, product=d, engine='All~~~')),                                          list(df_product_new['year']), list(df_product_new['makeID']),                                          list(df_product_new['modelID']), list(df_product_new['productID']))


    # #list of all sub dictionaries
    # values = []

    # #dict of individual sub element data
    # sub = {}
    # cnt = 0
    # cnter = 0

    # for year, make, model, product, url in year_make_model_product_engine_url:

    #     try:
    #         #feed url to return xml doc and load into etree
    #         data = requests.get(url)
    #         tree = etree.XML(data.content)

    #         #iterate over each element and create dicts based on values
    #         el_list = ['supplier',
    #                     'app_no',
    #                     'vehicle',
    #                     'location',
    #                     'brand',
    #                     'part_no',
    #                     'part_type',
    #                     'part_key',
    #                     'comment',
    #                     'application',
    #                     'qty',
    #                     'case_qty',
    #                     'price',
    #                     'storelocator',
    #                     'doc',
    #                     'udf2',
    #                     'udf1',
    #                     'displayorder',
    #                     'supplierid',
    #                     'image',
    #                     'epicor_id',
    #                     'epicor_name',
    #                     'smtp_brandid',
    #                     'id']
            
    #         for el in tree.iter(el_list):
    #             cnt += 1
    #             sub[el.tag] = el.text

    #             if cnt == len(el_list):
    #                 values.append(sub)
    #                 sub = {}
    #                 cnt = 0
                    
    #         cnter+=1
    #         if cnter % 1000 == 0:
    #             #finally, create a dataframe
    #             df_wrking = pd.DataFrame(values)
    #             print "{:d} values loaded".format(len(values))
    #             dumpToCSV()
    #             time.sleep(0.1)

    #     except:
    #         print "ERROR******{:s}".format(url)

    # #finally, create a dataframe
    # df_parts = pd.DataFrame(values)
    # df_wrking = None
    # dumpToCSV()


    # # #api pieces remaining
    # # *parts
    # # *partimage
    # # *partdetail
    # # *app
    # # *doc
    # # *kit
    # # *swf
    # # *360IMAGES

    # # **PARTS DETAIL**

    # # In[49]:

    # df_parts_new[df_parts_new['part_key'] == 1090687].head()


    # # In[53]:

    # df_parts_new = df_parts.drop_duplicates(subset='part_key', keep='last')
    # parts_url = map(lambda a: (a, qry_smtp_encode(lookup='partdetail', part=a)),list(df_parts_new['part_key']))


    # # In[52]:

    # df_parts_new


    # # In[47]:

    # parts_url


    # # In[ ]:

    # df_parts_new = df_parts
    # # try:
    # #     df_product_new = chk_exsting_vals(df_parts, df_engine)
    # # except NameError:
    # #     df_product_new = df_product

    # #create list of tuples of urls, years, mak`es, model from df_product
    # parts_url = map(lambda a: (a, qry_smtp_encode(lookup='parts', part=a)), list(df_parts_new['part_key'])

    # #list of all sub dictionaries
    # values = []

    # #dict of individual sub element data
    # sub = {}
    # cnt = 0
    # cnter = 0

    # for part, url in parts_url:

    #     try:
    #         #feed url to return xml doc and load into etree
    #         data = requests.get(url)
    #         tree = etree.XML(data.content)

    #         #iterate over each element and create dicts based on values
    #         el_list = ['attribute', 'value', 'recno']
            
    #         for el in tree.iter(el_list):
    #             cnt += 1
    #             sub[el.tag] = el.text

    #             if cnt == len(el_list):
    #                 sub['partID'] = part
    #                 values.append(sub)
    #                 sub = {}
    #                 cnt = 0
                    
    #         cnter+=1
    #         if cnter % 1000 == 0:
    #             #finally, create a dataframe
    #             df_wrking = pd.DataFrame(values)
    #             print "{:d} values loaded".format(len(values))
    #             dumpToCSV()
    #             time.sleep(0.1)

    #     except:
    #         print "ERROR******{:s}".format(url)

    # #finally, create a dataframe
    # df_partsdetail = pd.DataFrame(values)
    # df_wrking = None
    # df_partsdetail.to_csv('data\partsdetail.csv')

