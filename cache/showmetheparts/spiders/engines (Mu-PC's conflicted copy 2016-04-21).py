from scrapy.spiders import XMLFeedSpider

from showmetheparts.items import EnginesItem
from showmetheparts.functions import createEngineURLs

from lxml import etree as et
import pandas as pd

# import pdb


class EnginesSpider(XMLFeedSpider):
    name = 'engines'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']

    cols = ['productID', 'makeID', 'modelID', 'year']
    data = pd.read_csv('../data/products.csv', usecols=cols)

    years = list(data['year'])
    makes = list(data['makeID'].astype(str).str.zfill(4))
    models = list(data['modelID'].astype(str).str.zfill(5))
    products = list(data['productID'].astype(str).str.zfill(5))

    start_urls = next(createEngineURLs(years, makes, models, products))

    iterator = 'iternodes'
    itertag = 'engine'  # change it accordingly

    def parse_node(self, response, selector):
        # pdb.set_trace()
        i = EnginesItem()
        i['year'] = et.XML(selector.xpath('yearid').extract()[0]).text
        i['make_id'] = et.XML(selector.xpath('makeid').extract()[0]).text
        i['model_id'] = et.XML(selector.xpath('modelid').extract()[0]).text
        i['product_id'] = "PLACEHOLDER"
        i['engine_id'] = et.XML(selector.xpath('id').extract()[0]).text
        i['engine'] = et.XML(selector.xpath('data').extract()[0]).text
        i['engnumeric_id'] = et.XML(
            selector.xpath('engineid').extract()[0]).text
        i['methodid'] = et.XML(selector.xpath('methodid').extract()[0]).text
        i['label'] = et.XML(selector.xpath('label').extract()[0]).text
        i['style'] = et.XML(selector.xpath('style').extract()[0]).text
        return i
