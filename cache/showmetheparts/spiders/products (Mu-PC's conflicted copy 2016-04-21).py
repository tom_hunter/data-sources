from scrapy.spiders import XMLFeedSpider

from showmetheparts.items import ProductsItem
from showmetheparts.functions import createProductsURLs

from lxml import etree as et
import pandas as pd

# import pdb


class ProductsSpider(XMLFeedSpider):
    name = 'products'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']

    years = list(pd.read_csv('../data/years.csv')['year'])
    makes = list(
        pd.read_csv('../data/makes.csv', usecols=['makeID'])['makeID'].astype(str).str.zfill(4))
    models = list(
        pd.read_csv('../data/models.csv', usecols=['modelID'])['modelID'].astype(str).str.zfill(5))

    start_urls = next(createProductsURLs(years, makes, models))
    iterator = 'iternodes'
    itertag = 'product'  # change it accordingly

    def parse_node(self, response, selector):
        # pdb.set_trace()
        i = ProductsItem()
        i['year'] = "YEAR"
        i['make_id'] = "MAKE_ID"
        i['model_id'] = "MODEL_ID"
        i['product'] = et.XML(selector.xpath('data').extract()[0]).text
        i['id'] = et.XML(selector.xpath('id').extract()[0]).text
        return i
