from scrapy.spiders import XMLFeedSpider

from showmetheparts.items import PartsItem
from showmetheparts.functions import createPartsURLs

from lxml import etree as et
import pandas as pd

# import pdb


class PartsSpider(XMLFeedSpider):
    name = 'parts'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']

    years = list(pd.read_csv('../data/years.csv')['year'])
    makes = list(
        pd.read_csv('../data/makes.csv', usecols=['makeID'])
        ['makeID'].astype(str).str.zfill(4))
    models = list(
        pd.read_csv('../data/models.csv', usecols=['modelID'])
        ['modelID'].astype(str).str.zfill(5))
    products = list(
        pd.read_csv('../data/products.csv', usecols=['productID'])
        ['productID'].astype(str).str.zfill(5))
    engines = list(
        pd.read_csv('../data/engines.csv', usecols=['engineID']))

    start_urls = next(createPartsURLs(years, makes, models, products, engines))
    iterator = 'iternodes'  # you can change this; see the docs
    itertag = 'partsdata'  # change it accordingly

    def parse_node(self, response, selector):
        # pdb.set_trace()
        i = PartsItem()
        i['year'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['makeid'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['modelid'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['productid'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['engine'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['supplier'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['app_no'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['vehicle'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['location'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['brand'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['part_no'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['part_type'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['part_key'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['comment'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['application'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['qty'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['case_qty'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['price'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['storelocator'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['doc'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['udf2'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['udf1'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['displayorder'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['supplierid'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['image'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['epicor_id'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['epicor_name'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['smtp_brandid'] = et.XML(selector.xpath('XXX').extract()[0]).text
        i['data'] = et.XML(selector.xpath('data').extract()[0]).text
        i['id'] = et.XML(selector.xpath('id').extract()[0]).text
        i['linecode'] = et.XML(selector.xpath('linecode').extract()[0]).text
        return i
