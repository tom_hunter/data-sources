from scrapy.spiders import XMLFeedSpider

from showmetheparts.items import SuppliersItem

from lxml import etree as et

# import pdb


class YearsSpider(XMLFeedSpider):

    name = 'years'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']
    start_urls = ["http://www.showmetheparts.com/bin/ShowMeConnect.Exe?lookup=year&id=091681"]
    iterator = 'iternodes'
    itertag = 'year'

    def parse_node(self, response, selector):
        # pdb.set_trace()
        i = SuppliersItem()
        i['id'] = et.XML(selector.xpath('id').extract()[0]).text
        return i
