from scrapy.spiders import XMLFeedSpider

from showmetheparts.items import SuppliersItem

from lxml import etree as et

# import pdb


class SuppliersSpider(XMLFeedSpider):
    name = 'suppliers'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']
    start_urls = ["http://www.showmetheparts.com/bin/ShowMeConnect.Exe?lookup=suppliers&Supplier=Full&id=091681"]
    iterator = 'iternodes'
    itertag = 'name'

    def parse_node(self, response, selector):
        i = SuppliersItem()
        i['data'] = et.XML(selector.xpath('data').extract()[0]).text
        i['id'] = et.XML(selector.xpath('id').extract()[0]).text
        i['linecode'] = et.XML(selector.xpath('linecode').extract()[0]).text
        return i
