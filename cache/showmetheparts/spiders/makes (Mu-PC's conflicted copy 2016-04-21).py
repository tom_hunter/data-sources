from scrapy.spiders import XMLFeedSpider

from showmetheparts.items import MakeItem
from showmetheparts.functions import createMakeURLs

from lxml import etree as et
import pandas as pd

# import pdb


class MakesSpider(XMLFeedSpider):
    name = 'makes'
    allowed_domains = ['http://www.showmetheparts.com/bin/ShowMeConnect.Exe']
    start_urls = next(createMakeURLs(
        list(pd.read_csv('../data/years.csv', usecols=['year'])['year'])))
    iterator = 'iternodes'
    itertag = 'make'  # change it accordingly

    def parse_node(self, response, selector):
        # pdb.set_trace()
        # how do I pass url parameter values? is it part of the response
        # object? request obj?
        i = MakeItem()
        i['year'] = "YEAR"
        i['id'] = et.XML(selector.xpath('id').extract()[0]).text
        i['make'] = et.XML(selector.xpath('data').extract()[0]).text
        return i
